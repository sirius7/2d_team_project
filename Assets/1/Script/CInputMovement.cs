﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputMovement : CMovement {

   
    public float _speed;
    
    CGameManager _gameManager;

    protected override void Awake()
    {
        base.Awake();
        
    }

	// Use this for initialization
	void Start () {
        _gameManager = GameObject.Find("GameManager").GetComponent<CGameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    protected virtual void Move()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // 가로 이동에 대해서
        if ((_characterState._isRightDir && h < 0) ||
            (!_characterState._isRightDir && h > 0))
        {
            Flip(); // CMove.Flip() 호출
        }

        // 입력 방향 벡터 설정
        Vector2 moveDirection = new Vector2(h, v);

        // 키 입력이 없다면
        if (moveDirection == Vector2.zero)
        {
            _animator.SetBool("Walk", false);
        }
        else  // 키 입력이 있다면
        {
            // 이동 애니메이션 재생
            _animator.SetBool("Walk", true);

           _CharacterTranform.Translate(moveDirection.normalized * _speed * Time.deltaTime);

            // 맵 가장자리 처리
            //if (_gameManager != null)
            //{
            //    _playerPos.position = _gameManager.GetPosIfMapCollision(_playerPos.position);
            //}
        }
        _CharacterTranform.position = new Vector2(Mathf.Clamp(_CharacterTranform.position.x, -7f, 7f), Mathf.Clamp(_CharacterTranform.position.y, -4f, 3f));
    }
}
