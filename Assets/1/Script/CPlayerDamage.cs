﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPlayerDamage : MonoBehaviour {

    CCharacterState _state;
    Animator _animator;

    private void Awake()
    {
        _state = GetComponent<CCharacterState>();
        _animator = GetComponent<Animator>();
    }

    public void Damage()
    {
        // 이미 데미지를 입은 상태라면 패스
        if (_state.state == CCharacterState.State.Damage
            || _animator.GetCurrentAnimatorStateInfo(1).IsName("Damage")
            ) return;

        Debug.Log("플레이어 데미지");

        // 피격 애니메이션을 실행함
        _animator.Play("Damage", 1);

        
    }
}
