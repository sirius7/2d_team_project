﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CStoneDestroyer : CDestroyer {

    // 파괴 이펙트
    public GameObject _destroyEffectPrefab;

    

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
    }
    public override void Destroy()
    {
        if (_destroyEffectPrefab != null)
        {
            // 오브젝트 파괴 이펙트 생성
            GameObject effect = Instantiate(_destroyEffectPrefab,
                transform.position, Quaternion.identity);
            Destroy(effect, 0.25f);
        }

        // 조각 증가
        _gm.DustUp(100);

        base.Destroy();
    }
}
