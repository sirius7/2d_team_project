﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMonsterDestroyer : CDestroyer {

    // 파괴 이펙트
    public GameObject _destroyEffectPrefab;

    public CGameManager _gameManager;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        _gameManager = GameObject.Find("GameManager").GetComponent<CGameManager>();
    }
    public override void Destroy()
    {
        if (_destroyEffectPrefab != null)
        {
            // 오브젝트 파괴 이펙트 생성
            GameObject effect = Instantiate(_destroyEffectPrefab,
                transform.position, Quaternion.identity);
            Destroy(effect, 0.25f);
        }

        // 하트 증가
        _gameManager.HeartUp(1);

        base.Destroy();
    }
}
