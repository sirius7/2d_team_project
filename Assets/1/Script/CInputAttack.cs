﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputAttack : MonoBehaviour
{

    Animator _animator;

    public Transform _attackPoint;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
        ToolChange();
    }

    void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")) return;

            _animator.SetTrigger("Attack");
        }
    }

    void ToolChange()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Debug.Log("도구 체인지 누름");
        }
    }

    // 공격 애니메이션 타이밍 이벤트 메소드
    public void AttackAnimationEvent()
    {
        DamageToMonster();
        DamageToStone();
    }

    void DamageToMonster()
    {
        //  공격 포인트에서 1 반지름 안에 들어오는 몬스터들의 콜라이더를 검출함
        Collider2D[] colliders = Physics2D.OverlapCircleAll(
            _attackPoint.position, 0.2f, 1 << LayerMask.NameToLayer("Monster"));  // 레이어 검출공식

        if (colliders.Length <= 0) return;

        // 피격이 검출된 몬스터들에게 피격 데미지를 줌
        foreach (Collider2D collider in colliders)
        {
            if (collider == null) continue; //  몬스터가 파괴되었을 수도 있음

            collider.SendMessage("Damage", 1f); // 주는 데미지
        }
    }

    void DamageToStone()
    {
        //  공격 포인트에서 1 반지름 안에 들어오는 몬스터들의 콜라이더를 검출함
        Collider2D[] colliders = Physics2D.OverlapCircleAll(
            _attackPoint.position, 0.2f, 1 << LayerMask.NameToLayer("Stone"));  // 레이어 검출공식

        if (colliders.Length <= 0) return;

        // 피격이 검출된 몬스터들에게 피격 데미지를 줌
        foreach (Collider2D collider in colliders)
        {
            if (collider == null) continue; //  몬스터가 파괴되었을 수도 있음

            collider.SendMessage("Damage", 1f); // 주는 데미지
        }
    }
}
