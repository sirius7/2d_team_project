﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPlayerItemState : MonoBehaviour {

    //  재료 수
    private int _materialCount;

    // 하트 수
    private int _heartCount;
    
    // 재료 획득
    public void MaterialUp(int value)
    {
        _materialCount += value;
    }

    //  재료 소비
    public int MaterialDown(int value)
    {
        _materialCount -= value;

        if (_materialCount < 0)
        {
            _materialCount = 0;
        }

        return _materialCount;
    }

    // 하트 획득
    public void HeartUp(int value)
    {
        _heartCount += value;
    }

    //  하트 소비
    public int HeartDown(int value)
    {
        _heartCount -= value;

        if (_heartCount < 0)
        {
            _heartCount = 0;
        }

        return _heartCount;
    }

    public int GetMaterialCount()
    {
        return _materialCount;
    }

    public int GetHeartCount()
    {
        return _heartCount;
    }
}
