﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGenerator : MonoBehaviour {

    // 맵 영역
    public Transform[] _areas;

    // 광석, 나무 등의 환경 요소
    public GameObject[] _environments;
    public int _environmentCount;   //  생성 숫자

    // 몬스터들
    public GameObject[] _monters;
    public int _monsterCount;   //  생성 숫자

    // Use this for initialization
    void Start () {
        EnvironmentGenerate();
    }

    public void EnvironmentGenerate()
    {
        int areaType = Random.Range(0, _areas.Length);

        Collider2D selCol = _areas[areaType].GetComponent<BoxCollider2D>();
        
        Debug.Log(areaType + " 번째 영역 최소 x : " + selCol.bounds.min.x);
        Debug.Log(areaType + " 번째 영역 최소 y : " + selCol.bounds.min.y);

        Debug.Log(areaType + " 번째 영역 최대 x : " + selCol.bounds.max.x);
        Debug.Log(areaType + " 번째 영역 최대 y : " + selCol.bounds.max.y);

        

        for (int i = 0; i < _monsterCount; i++)
        {
            int monType = Random.Range(0, _monters.Length);

            float ranPosX = Random.Range(selCol.bounds.min.x
            , selCol.bounds.max.x);

            float ranPosY = Random.Range(selCol.bounds.min.y
            , selCol.bounds.max.y);

            Debug.Log("생성 위치 x : " + ranPosX);
            Debug.Log("생성 위치 y : " + ranPosY);

            Vector2 genPos = new Vector2(ranPosX, ranPosY);

            Instantiate(_monters[monType], genPos, Quaternion.identity);
        }

        for (int i = 0; i < _environmentCount; i++)
        {
            int enviType = Random.Range(0, _environments.Length);

            float ranPosX = Random.Range(selCol.bounds.min.x
            , selCol.bounds.max.x);

            float ranPosY = Random.Range(selCol.bounds.min.y
            , selCol.bounds.max.y);

            Debug.Log("생성 위치 x : " + ranPosX);
            Debug.Log("생성 위치 y : " + ranPosY);

            Vector2 genPos = new Vector2(ranPosX, ranPosY);

            Instantiate(_environments[enviType], genPos, Quaternion.identity);
        }
    }
}
