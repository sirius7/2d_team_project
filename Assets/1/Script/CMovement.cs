﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMovement : MonoBehaviour {

    protected Transform _CharacterTranform;

    protected CCharacterState _characterState; // 캐릭터 상태
    protected  Animator _animator;
    protected virtual void Awake()
    {
        _CharacterTranform = this.gameObject.GetComponent<Transform>();
        _characterState = GetComponent<CCharacterState>();

        _animator = GetComponent<Animator>();
    }

    // 방향 전환
    public void Flip()
    {
        // 오브젝트 반전
        Vector3 theScale = _CharacterTranform.localScale;
        theScale.x *= -1;
        _CharacterTranform.localScale = theScale;

        // 시선 정보 변경
        _characterState._isRightDir = !_characterState._isRightDir;
    }
}
