﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEnemyHealth : MonoBehaviour {

    public int _hp;

    public int GetHp()
    {
        return _hp;
    }
}
