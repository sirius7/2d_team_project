﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPlayerDamage_sirius : MonoBehaviour {

    CPlayerStatus _cps;
    
    private void Awake()
    {
        _cps = GetComponent<CPlayerStatus>();

    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if(_cps.whereisPlayer.Equals("world"))
        {
            if (_cps.hp <= 0)
                return;

            DamagedFromBlizzard(_cps.coldResist);
        }
    }

    public void DamagedFromBlizzard(float coldres)
    {
        _cps.hp -= ((1 - coldres) * Time.deltaTime);
    }
}
