﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputAttack_sirius : MonoBehaviour {

    public Transform _attackPoint;
    Animator _anim;

    
    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }
    
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if(!_anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                _anim.SetTrigger("Attack");
        }
	}

    public void Attack()
    {
        Collider2D hitCol = Physics2D.OverlapCircle(_attackPoint.position, 0.3f, 1 << LayerMask.NameToLayer("Monster"));
    }
}
