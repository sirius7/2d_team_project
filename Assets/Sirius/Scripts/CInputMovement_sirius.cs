﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInputMovement_sirius : MonoBehaviour {


    public float _speed;

    private Transform player;

    float h, v;
    Vector2 direction;

    private void Awake()
    {
        player = this.gameObject.GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update () {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");

        direction = new Vector2(h, v);
	}

    private void FixedUpdate()
    {
        player.Translate(direction * _speed * Time.fixedDeltaTime);
        player.position = new Vector2(Mathf.Clamp(player.position.x,-7f,7f), Mathf.Clamp(player.position.y, -4f,3f));
    }
}
