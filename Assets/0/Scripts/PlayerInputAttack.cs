﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputAttack : MonoBehaviour {

    public Animator _animator;

    public Transform _attackPoint; // 공격 타격 위치 포인트

    public int _attackTargetMask; // 충돌 대상 레이어 마스크

    

    // Use this for initialization
    void Awake () {

        _animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {

        Attack();
        

    }

    void Attack()
    {
        

        if (Input.GetKeyDown(KeyCode.Z))
        {
            // 스윙 애니메이션을 실행 해라
            _animator.SetTrigger("Attack");
        }

    }


    public void AttackAnimationEvent()
    {
        //공격 포인트에서 1반지름 안에 들어오는 몬스터들의 콜라이더를 검출
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_attackPoint.position, 1f, 1 << LayerMask.NameToLayer("Monster"));

        if (colliders.Length <= 0) return;

        //피격이 검출된 첫번째 몬스터에게 피격데미지를 줌
        foreach (Collider2D collider in colliders)
        {
            if (collider == null) continue;
            //collider.GetComponent<CMonsterDamage>().Damage(1f);
            collider.SendMessage("Damage", 30f);

            break;
        }

    }
}
