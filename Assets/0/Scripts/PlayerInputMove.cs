﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputMove : MonoBehaviour {

    public float _speed;

    Animator _animator;

    public bool _isRightFace;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Vector2 direction = new Vector2(h, v);

        if (direction == Vector2.zero)
        {
            _animator.SetBool("Walk", false);
        }
        else  // 키 입력이 있다면
        {
            // 이동 애니메이션 재생
            _animator.SetBool("Walk", true);

        }

        if ((h < 0 && !_isRightFace) || (h > 0 && _isRightFace))
        {
            Flip(); // 캐릭터를 반전 해라
        }

        AnimatorStateInfo animState = _animator.GetCurrentAnimatorStateInfo(0);

        if (!animState.IsName("Idle") && !animState.IsName("Walk"))
        {
            return;
        }

        transform.Translate(direction * _speed * Time.deltaTime);
    }
    


    public void Flip()
    {
        // Transform의 스케일 벡터 받음
        Vector3 scale = transform.localScale;
        // 시선 반전
        scale.x = scale.x * (-1); // scale.x *= -1;
        transform.localScale = scale;

        // 시선값 반전
        _isRightFace = !_isRightFace;
    }
}


