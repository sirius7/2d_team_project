﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDamage : MonoBehaviour {

    CharacterState _monsterState;

    

    public GameObject _damageEffectPrefab;

    Animator _animator;

    Vector3 _direction; // 이동 방향



    // Use this for initialization
    void Awake () {

        _monsterState = GetComponent<CharacterState>();
        _animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Damage(float damage)
    {
        _monsterState._hp -= damage;

        _animator.Play("Damage", 1);

        if (_monsterState._hp <= 0)
        {
           
            _monsterState._isDie = true;

            _animator.Play("Die");

            transform.Translate(_direction.normalized * 0f * Time.deltaTime);

            Destroy(gameObject, 2f);

        }

    }
}
