﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMovement : MonoBehaviour {

    Vector3 _direction; // 이동 방향

    CharacterState _monsterState;

    public float _speed;  // 이동 속도

    public bool _isRightFace; // 시선

    Animator _animator;

    public Transform _target = null;

    public float _checkRange;

    protected Transform _attackPoint;

    

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _attackPoint = transform.Find("AttackPoint").transform;
    }

    // Use this for initialization
    void Start () {



       

        // GameOject.Find("오브젝트이름") : 지정한 이름을 가진 오브젝트를 찾아줌
        GameObject player = GameObject.Find("Player");

        // GameObject.GetComponent<컴포넌트타입>()
        // : 게임오브젝트에 속한 여러개의 컴포넌트중에 
        // 지정한 타입의 컴포넌트를 구함
        Transform playerTr = player.GetComponent<Transform>();

        // 방향 = 이동목적지위치 - 이동대상의위치
        _direction = playerTr.position - transform.position;
        
        // 좀비가 생성된 위치가 왼쪽일 경우
        if ((_target.position.x - transform.position.x) < 0)
        {
            Flip(); // 캐릭터를 반전 시킴
        }
        




    }
	
	// Update is called once per frame
	void Update () {


        TargetCheck();

        

        




    }

    public void Flip()
    {
        // Transform의 스케일 벡터 받음
        Vector3 scale = transform.localScale;
        // 시선 반전
        scale.x = scale.x * (-1); // scale.x *= -1;
        transform.localScale = scale;

        // 시선값 반전
        _isRightFace = !_isRightFace;
    }


    public void TargetCheck()
    {
        // 추적할 타겟을 잃었다면
        if (_target == null)
        {
            _animator.SetBool("Idle", true);

            transform.Translate(_direction.normalized * 0f * Time.deltaTime);

            // 처음 생성시 설정된 방향으로 이동함
            //transform.Translate(_direction.normalized * _speed * Time.deltaTime);

        }

        float distance = Vector2.Distance(_attackPoint.position, _target.transform.position);

        if (distance < _checkRange)
        {
            _animator.SetBool("Idle", false);
            _animator.SetBool("Walk", true);

            // 타겟의 방향을 다시 구해서 이동함
            _direction = _target.position - transform.position;



            transform.Translate(_direction.normalized * _speed * Time.deltaTime);
        }

        else if (distance > _checkRange)
        {
            _animator.SetBool("Idle", true);
            _animator.SetBool("Walk", false);

            transform.Translate(_direction.normalized * 0f * Time.deltaTime);
        }
    }

    public void MoveStop()
    {
        _speed = 0;
    }
}
