﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAttack : MonoBehaviour {

    CharacterState _monsterState;

    public GameObject _attackTarget;

    public Transform _attackPoint;

    Animator _animator;
    

    // Use this for initialization
    void Awake () {

        _attackTarget = GameObject.FindGameObjectWithTag("Player");
        _attackPoint = transform.Find("AttackPoint").transform;
        _animator = GetComponent<Animator>();
        _monsterState = GetComponent<CharacterState>();

    }
	
	// Update is called once per frame
	void Update () {
        
		
	}

    public void AttackReady()
    {
        _monsterState.state = CharacterState.State.Attack;

        _animator.SetBool("Attack", true);
    }

    public void Attack()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_attackPoint.position, 3f, _monsterState._targetMask);

        foreach (Collider2D collider in colliders)
        {
            if (collider.tag == "Player")
            {
                collider.SendMessage("Damage", SendMessageOptions.DontRequireReceiver);
               

                return;
            }

        }
        
    }

    public void AttackFinish()
    {
        _monsterState.state = CharacterState.State.Idle;
        _animator.SetBool("Attack", false);
    }


}
