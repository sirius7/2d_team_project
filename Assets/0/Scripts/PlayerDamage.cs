﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour {

    public int _hp;

    public bool _isDie = false;

    Animator _animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Zombie"))
        {
            // 이미 피격을 당해 피해를 입고 있는 상태라면 패쓰~
            if (_animator.GetCurrentAnimatorStateInfo(1).IsName("Damage")) return;

            HpDown(30); // 체력 감소

            // 데미지 애니메이션을 수행함
            _animator.Play("Damage");
        }
    }

    public int HpDown(float damage)
    {
        _hp -= (int)damage;

        return _hp;

        if (_hp <= 0)
        {
            _isDie = true;
        }
    }

    // 사망 처리
    public void Die()
    {
        // 사망 여부를 설정함
        _isDie = true;

        // 피격 콜라이더를 비활성화 함
        GetComponent<BoxCollider2D>().enabled = false;

        _animator.SetTrigger("Die"); // 사망 애니메이션 실행

    }

}
